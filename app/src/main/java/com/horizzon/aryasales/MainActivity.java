package com.horizzon.aryasales;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.horizzon.aryasales.adepter.AreaListAdapter;
import com.horizzon.aryasales.adepter.CityList_Adapter;
import com.horizzon.aryasales.adepter.CityList_Data;
import com.horizzon.aryasales.adepter.StateListAdapter;
import com.horizzon.aryasales.model.AreaList_Data;
import com.horizzon.aryasales.model.AreaList_Response;
import com.horizzon.aryasales.model.CityListResponse;
import com.horizzon.aryasales.model.StateList_Data;
import com.horizzon.aryasales.model.StateList_Response;
import com.horizzon.aryasales.retrofit.APIClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity  {

    Spinner state_spinner, city_spinner, area_spinner;
    TextView view;
        List<StateList_Data> state_list;
    List<CityList_Data>citylist;
    List<AreaList_Data> arealist;
    StateListAdapter stateListAdapter;
    CityList_Adapter cityList_adapter;
    AreaListAdapter areaListAdapter;
    String state_id="1",city_id="1",area_id="1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        state_spinner = findViewById(R.id.state_spinner);
        city_spinner = findViewById(R.id.city_spinner);
        area_spinner = findViewById(R.id.area_spinner);
        view = findViewById(R.id.view);

        state_list = new ArrayList<>();
        getstateList();


        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                StateList_Data data = stateListAdapter.getItem(position);
                // Here you can do the action you want to...
                state_id=data.getId();
                Toast.makeText(MainActivity.this, ""+state_id, Toast.LENGTH_SHORT).show();
                 citylist = new ArrayList<>();
                getCityList(state_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });


        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CityList_Data data = cityList_adapter.getItem(position);
                city_id=data.getId();

                Toast.makeText(MainActivity.this, "state id"+state_id+"city id"+city_id, Toast.LENGTH_SHORT).show();
                arealist = new ArrayList<>();
                getAreaList(state_id,city_id);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        area_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }



    private void getCityList(String id) {

        Call<CityListResponse> call = APIClient.getInterface().getCityList(id);
        call.enqueue(new Callback<CityListResponse>() {
            @Override
            public void onResponse(Call<CityListResponse> call, Response<CityListResponse> response) {
                if (response.isSuccessful()) {




                    if (response.body().getResponseCode().equals("200")) {

                        citylist = response.body().getData();
                        cityList_adapter = new CityList_Adapter(MainActivity.this, android.R.layout.simple_spinner_item, citylist);
                        city_spinner.setAdapter(cityList_adapter);

                    }else if (response.body().getResponseCode().equals("401")){
                        citylist.clear();
                        getAreaList(state_id,city_id);
                        cityList_adapter = new CityList_Adapter(MainActivity.this, android.R.layout.simple_spinner_item, citylist);
                        city_spinner.setAdapter(cityList_adapter);
                        cityList_adapter.notifyDataSetChanged();
                        Toast.makeText(MainActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CityListResponse> call, Throwable t) {

            }
        });
    }

    private void getstateList() {
        Call<StateList_Response> call = APIClient.getInterface().getStateList();
        call.enqueue(new Callback<StateList_Response>() {
            @Override
            public void onResponse(Call<StateList_Response> call, Response<StateList_Response> response) {
                if (response.isSuccessful()) {



                    if (response.body().getResponseCode().equals("200")){
                        state_list = response.body().getData();
                        stateListAdapter = new StateListAdapter(MainActivity.this,
                                android.R.layout.simple_spinner_item,
                                state_list);
                        state_spinner.setAdapter(stateListAdapter);
                        stateListAdapter.notifyDataSetChanged(); // Set the custom adapter to the spinner
                    }else if (response.body().getResponseCode().equals("401")){
                        state_list.clear();
                        stateListAdapter = new StateListAdapter(MainActivity.this,
                                android.R.layout.simple_spinner_item,
                                state_list);
                        state_spinner.setAdapter(stateListAdapter);
                        stateListAdapter.notifyDataSetChanged();
                        Toast.makeText(MainActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<StateList_Response> call, Throwable t) {

            }
        });


        // You can create an anonymous listener to handle the event when is selected an spinner item




    }
    private void getAreaList(String state_id, String city_id) {

        Call<AreaList_Response>call=APIClient.getInterface().getAreaList(state_id,city_id);

        call.enqueue(new Callback<AreaList_Response>() {
            @Override
            public void onResponse(Call<AreaList_Response> call, Response<AreaList_Response> response) {
                if (response.isSuccessful()) {



                    if (response.body().getResponseCode().equals("200")) {

                      if (citylist.size()>0){
                          arealist = response.body().getData();
                          areaListAdapter = new AreaListAdapter(MainActivity.this,
                                  android.R.layout.simple_spinner_item,
                                  arealist);
                          area_spinner.setAdapter(areaListAdapter);
                      }else {
                          arealist.clear();
                          areaListAdapter = new AreaListAdapter(MainActivity.this,
                                  android.R.layout.simple_spinner_item,
                                  arealist);
                          area_spinner.setAdapter(areaListAdapter);
                          areaListAdapter.notifyDataSetChanged();
                      }

                    }
                    else if (response.body().getResponseCode().equals("401")){
                        arealist.clear();
                        arealist=new ArrayList<>();
                        areaListAdapter = new AreaListAdapter(MainActivity.this,
                                android.R.layout.simple_spinner_item,
                                arealist);
                        area_spinner.setAdapter(areaListAdapter);
                        areaListAdapter.notifyDataSetChanged();
                        Toast.makeText(MainActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AreaList_Response> call, Throwable t) {

            }
        });
    }


}
