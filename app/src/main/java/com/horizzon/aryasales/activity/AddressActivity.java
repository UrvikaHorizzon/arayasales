package com.horizzon.aryasales.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.Selection;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.horizzon.aryasales.MainActivity;
import com.horizzon.aryasales.R;
import com.horizzon.aryasales.adepter.AddressCategoryTypeAdapter;
import com.horizzon.aryasales.adepter.AreaListAdapter;
import com.horizzon.aryasales.adepter.CityList_Adapter;
import com.horizzon.aryasales.adepter.CityList_Data;
import com.horizzon.aryasales.adepter.StateListAdapter;
import com.horizzon.aryasales.model.Address;
import com.horizzon.aryasales.model.AddressTypeModel;
import com.horizzon.aryasales.model.Area;
import com.horizzon.aryasales.model.AreaD;
import com.horizzon.aryasales.model.AreaList_Data;
import com.horizzon.aryasales.model.AreaList_Response;
import com.horizzon.aryasales.model.CityListResponse;
import com.horizzon.aryasales.model.StateList_Data;
import com.horizzon.aryasales.model.StateList_Response;
import com.horizzon.aryasales.model.UpdateAddress;
import com.horizzon.aryasales.model.User;
import com.horizzon.aryasales.retrofit.APIClient;
import com.horizzon.aryasales.retrofit.GetResult;
import com.horizzon.aryasales.utils.SessionManager;
import com.horizzon.aryasales.utils.Utiles;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.horizzon.aryasales.utils.Utiles.isRef;

public class AddressActivity extends BaseActivity implements GetResult.MyListener {
    @BindView(R.id.ed_username)
    EditText edUsername;
    @BindView(R.id.ed_type)
    EditText edType;
    @BindView(R.id.ed_landmark)
    EditText edLandmark;
    SessionManager sessionManager;
    @BindView(R.id.ed_hoousno)
    EditText edHoousno;
    @BindView(R.id.ed_society)
    EditText edSociety;
    @BindView(R.id.ed_pinno)
    EditText edPinno;
    String areaSelect;

    @BindView(R.id.state_spinner)
    Spinner state_spinner;
    @BindView(R.id.city_spinner)
    Spinner city_spinner;
    @BindView(R.id.area_spinner)
    Spinner area_spinner;
    @BindView(R.id.spinnerAddresaType)
    Spinner spinnerAddresaType;
    @BindView(R.id.txtarea_spinner)
    EditText txtarea_spinner;

    @BindView(R.id.ed_GST_num)
    EditText ed_GST_num;
    @BindView(R.id.edtShopName)
    EditText edtShopName;
    User user;
    Address address;
    String itemSelected, GTSNumber, ShopName;
    List<String> categories = new ArrayList<String>();
    private List<AddressTypeModel> values = new ArrayList<>();
    List<StateList_Data> state_list = new ArrayList<>();
    List<CityList_Data> citylist;
    List<AreaList_Data> arealist;
    StateListAdapter stateListAdapter;
    AddressCategoryTypeAdapter addressCategoryTypeAdapter;
    CityList_Adapter cityList_adapter;
    AreaListAdapter areaListAdapter;

    int stateId, CityId;
    String state_name = "", city_name = "", area_name = "";
    String state_id = "", city_id = "", area_id = "";
    SharedPreferences sharedPreferences;
    int selectedMathod;
    String AddressType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        ButterKnife.bind(this);

        getstateList();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sessionManager = new SessionManager(AddressActivity.this);
        user = sessionManager.getUserDetails("");
        address = (Address) getIntent().getSerializableExtra("MyClass");
        // GetArea();
        if (address != null)
            setcountaint(address);
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                areaSelect = areaDS.get(position).getName();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        citylist = new ArrayList<>();
        arealist = new ArrayList<>();


        AddressTypeModel movie = new AddressTypeModel("Direct customer");
        values.add(movie);
        movie = new AddressTypeModel("Distributor");
        values.add(movie);
        movie = new AddressTypeModel("Dealer");
        values.add(movie);
        movie = new AddressTypeModel("Chemist");
        values.add(movie);
        movie = new AddressTypeModel("Ayurvedic shope");
        values.add(movie);
        movie = new AddressTypeModel("General kirana");
        values.add(movie);
        movie = new AddressTypeModel("Other Specific");
        values.add(movie);

        addressCategoryTypeAdapter = new AddressCategoryTypeAdapter(AddressActivity.this,
                android.R.layout.simple_spinner_item,
                values);
        spinnerAddresaType.setAdapter(addressCategoryTypeAdapter);
        if (AddressType != null) {
            for (int i = 0; i < values.size(); i++) {
                if (values.get(i).getStatus().matches(AddressType)) {
                    spinnerAddresaType.setId(i);
                    spinnerAddresaType.setSelection(i);
                }
            }
        }
        addressCategoryTypeAdapter.notifyDataSetChanged();

        spinnerAddresaType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).toString().equals("Other Specific")) {
                    edType.setVisibility(View.VISIBLE);
                    itemSelected = edType.getText().toString();
                } else {
                    edType.setVisibility(View.GONE);
                    itemSelected = parent.getItemAtPosition(position).toString();
                }

                if (!parent.getItemAtPosition(position).toString().equals("Direct customer")) {
                    ed_GST_num.setVisibility(View.VISIBLE);
                    edtShopName.setVisibility(View.VISIBLE);
                    GTSNumber = ed_GST_num.getText().toString();
                    ShopName = edtShopName.getText().toString();
                } else {
                    ed_GST_num.setVisibility(View.GONE);
                    edtShopName.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                StateList_Data data = stateListAdapter.getItem(position);
                // Here you can do the action you want to...
                state_id = data.getId();
                state_name = data.getStatename();
                sessionManager.setStateId(state_id, state_name);
                getCityList(state_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });


        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CityList_Data data = cityList_adapter.getItem(position);
                city_id = data.getId();
                city_name = data.getCityname();
                sessionManager.setCityId(city_id, city_name);
                //  getAreaList(sessionManager.getStateId(), city_id);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


      /*  area_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AreaList_Data data = new AreaList_Data();
                data = areaListAdapter.getItem(position);
                area_name = data.getName();
                sessionManager.setAreaId(data.getId(), area_name);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences.registerOnSharedPreferenceChangeListener(sharedPreferenceChangeListener);
    }

    private void setcountaint(Address address) {
        Log.d("TAG", "setcountaint: city : " + address.getCity());
        Log.d("TAG", "setcountaint: state : " + address.getState());
        edUsername.setText("" + address.getName());
        edType.setText("" + address.getName());
        edHoousno.setText("" + address.getHno());
        edSociety.setText("" + address.getSociety());
        edPinno.setText("" + address.getPincode());
        edLandmark.setText("" + address.getLandmark());
        txtarea_spinner.setText(address.getArea());
        AddressType = address.getType();
        Log.d("TAG", "setcountaint: " + address.getType());

        if (AddressType.equals("Direct customer")) {
            edType.setVisibility(View.GONE);
            edtShopName.setVisibility(View.GONE);
            ed_GST_num.setVisibility(View.GONE);
            selectedMathod = 0;
        } else if (AddressType.equals("Distributor")) {
            edType.setVisibility(View.GONE);
            edtShopName.setVisibility(View.VISIBLE);
            ed_GST_num.setVisibility(View.VISIBLE);
            ed_GST_num.setText(address.getGTSNumber());
            edtShopName.setText(address.getShopName());
            selectedMathod = 1;
        } else if (AddressType.equals("Dealer")) {
            edType.setVisibility(View.GONE);
            selectedMathod = 2;
            edtShopName.setVisibility(View.VISIBLE);
            ed_GST_num.setVisibility(View.VISIBLE);
            ed_GST_num.setText(address.getGTSNumber());
            edtShopName.setText(address.getShopName());
        } else if (AddressType.equals("Chemist")) {
            edType.setVisibility(View.GONE);
            selectedMathod = 3;
            edtShopName.setVisibility(View.VISIBLE);
            ed_GST_num.setVisibility(View.VISIBLE);
            ed_GST_num.setText(address.getGTSNumber());
            edtShopName.setText(address.getShopName());
        } else if (AddressType.equals("Ayurvedic shope")) {
            edType.setVisibility(View.GONE);
            selectedMathod = 4;
            edtShopName.setVisibility(View.VISIBLE);
            ed_GST_num.setVisibility(View.VISIBLE);
            ed_GST_num.setText(address.getGTSNumber());
            edtShopName.setText(address.getShopName());
        } else if (AddressType.equals("General kirana")) {
            edType.setVisibility(View.GONE);
            selectedMathod = 5;
            edtShopName.setVisibility(View.VISIBLE);
            ed_GST_num.setVisibility(View.VISIBLE);
            ed_GST_num.setText(address.getGTSNumber());
            edtShopName.setText(address.getShopName());
        } else {
            selectedMathod = 6;
            edType.setVisibility(View.VISIBLE);
            edType.setText(AddressType);
            edtShopName.setVisibility(View.VISIBLE);
            ed_GST_num.setVisibility(View.VISIBLE);
            ed_GST_num.setText(address.getGTSNumber());
            edtShopName.setText(address.getShopName());
        }


        stateId = Integer.parseInt(address.getState());
        CityId = Integer.parseInt(address.getCity());

    }

    private void GetArea() {
        JSONObject jsonObject = new JSONObject();
        JsonParser jsonParser = new JsonParser();
        Call<JsonObject> call = APIClient.getInterface().getArea((JsonObject) jsonParser.parse(jsonObject.toString()));
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "2");
    }

    @OnClick(R.id.txt_save)
    public void onViewClicked() {
        if (validation()) {
            if (address != null) {
                UpdateUser(address.getId());
            } else {

                UpdateUser("0");
            }
        }
    }

    private void UpdateUser(String aid) {
        String shopeName = edtShopName.getText().toString();
        String gstNumber = ed_GST_num.getText().toString();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            jsonObject.put("aid", aid);
            jsonObject.put("name", edUsername.getText().toString());
            jsonObject.put("hno", edHoousno.getText().toString());
            jsonObject.put("society", edSociety.getText().toString());
            jsonObject.put("area", txtarea_spinner.getText().toString());
            jsonObject.put("city", sessionManager.getCityId());
            jsonObject.put("state", sessionManager.getStateId());
            jsonObject.put("landmark", edLandmark.getText().toString());
            jsonObject.put("pincode", edPinno.getText().toString());
            jsonObject.put("type", itemSelected);
            jsonObject.put("mobile", user.getMobile());
            jsonObject.put("password", user.getPassword());
            jsonObject.put("gts_number", gstNumber);
            jsonObject.put("shop_name", shopeName);
            jsonObject.put("imei", Utiles.getIMEI(AddressActivity.this));
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().UpdateAddress((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void callback(JsonObject result, String callNo) {
        try {
            if (callNo.equalsIgnoreCase("1")) {
                Gson gson = new Gson();
                UpdateAddress response = gson.fromJson(result.toString(), UpdateAddress.class);
                Toast.makeText(AddressActivity.this, "" + response.getResponseMsg(), Toast.LENGTH_LONG).show();
                if (response.getResult().equals("true")) {
                    sessionManager.setAddress("", response.getAddress());
                    isRef = true;
                    finish();
                }
            } else if (callNo.equalsIgnoreCase("2")) {
                //Gson gson = new Gson();
                // Area area = gson.fromJson(result.toString(), Area.class);
//                areaDS = area.getData();
//                List<String> Arealist = new ArrayList<>();
//                for (int i = 0; i < areaDS.size(); i++) {
//                    if (areaDS.get(i).getStatus().equalsIgnoreCase("1")) {
//                        Arealist.add(areaDS.get(i).getName());
//                    }
//                }
//                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Arealist);
//                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                spinner.setAdapter(dataAdapter);
//                int spinnerPosition = dataAdapter.getPosition(address.getArea());
//                spinner.setSelection(spinnerPosition);
            }
        } catch (Exception e) {
            Log.d("TAG", "callback: " + e.getMessage());
        }
    }

    public boolean validation() {
        if (edUsername.getText().toString().isEmpty()) {
            edUsername.setError("Enter Name");
            return false;
        }


        if (edHoousno.getText().toString().isEmpty()) {
            edHoousno.setError("Enter House No");
            return false;
        }
        if (edSociety.getText().toString().isEmpty()) {
            edSociety.setError("Enter Society");
            return false;
        }


        if (state_spinner != null && state_spinner.getSelectedItem() != null) {
        } else {
            Toast.makeText(this, "state name is not insert", Toast.LENGTH_SHORT).show();
        }

        if (city_spinner != null && city_spinner.getSelectedItem() != null) {
        } else {
            Toast.makeText(this, "city name not insert", Toast.LENGTH_SHORT).show();
        }


       /* if (area_spinner != null && area_spinner.getSelectedItem() != null) {
        } else {
            Toast.makeText(this, "area name not insert", Toast.LENGTH_SHORT).show();
        }*/
        if (edLandmark.getText().toString().isEmpty()) {
            edLandmark.setError("Enter Landmark");
            return false;
        }

        if (edPinno.getText().toString().isEmpty()) {
            edPinno.setError("Enter Pincode");
            return false;
        }
        return true;
    }

    private static boolean validatePhoneNumber(String phoneNo) {
        //validate phone numbers of format "1234567890"
        if (phoneNo.matches("\\d{10}")) return true;
            //validating phone number with -, . or spaces
        else if (phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
            //validating phone number with extension length from 3 to 5
        else if (phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
            //validating phone number where area code is in braces ()
        else if (phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
            //return false if nothing matches the input
        else return false;
    }


    private void getCityList(String id) {

        Call<CityListResponse> call = APIClient.getInterface().getCityList(id);
        call.enqueue(new Callback<CityListResponse>() {
            @Override
            public void onResponse(Call<CityListResponse> call, Response<CityListResponse> response) {
                if (response.isSuccessful()) {


                    if (response.body().getResponseCode().equals("200")) {

                        citylist = response.body().getData();
                        cityList_adapter = new CityList_Adapter(AddressActivity.this, android.R.layout.simple_spinner_item, citylist);
                        if (CityId > 0) {
                            for (int i = 0; i < citylist.size(); i++) {
                                if (CityId == Integer.parseInt(citylist.get(i).getId())) {
                                    city_spinner.setSelection(i);
                                }
                            }
                        }

                        city_spinner.setAdapter(cityList_adapter);

                    } else if (response.body().getResponseCode().equals("401")) {
                        citylist.clear();
                        citylist = new ArrayList<>();
                        // getAreaList(state_id, city_id);
                        cityList_adapter = new CityList_Adapter(AddressActivity.this, android.R.layout.simple_spinner_item, citylist);
                        city_spinner.setAdapter(cityList_adapter);
                        cityList_adapter.notifyDataSetChanged();
                        Toast.makeText(AddressActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CityListResponse> call, Throwable t) {

            }
        });
    }

    private void getstateList() {
        Call<StateList_Response> call = APIClient.getInterface().getStateList();
        call.enqueue(new Callback<StateList_Response>() {
            @Override
            public void onResponse(Call<StateList_Response> call, Response<StateList_Response> response) {
                if (response.isSuccessful()) {


                    if (response.body().getResponseCode().equals("200")) {
                        state_list = response.body().getData();
                        stateListAdapter = new StateListAdapter(AddressActivity.this,
                                android.R.layout.simple_spinner_item,
                                state_list);
                        state_spinner.setAdapter(stateListAdapter);

                        for (int i = 0; i < state_list.size(); i++) {
                            if (Integer.parseInt(state_list.get(i).getId()) == stateId) {
                                state_spinner.setId(i);
                                state_spinner.setSelection(i);
                            }
                        }
                        stateListAdapter.notifyDataSetChanged(); // Set the custom adapter to the spinner
                    } else if (response.body().getResponseCode().equals("401")) {
                        state_list.clear();
                        stateListAdapter = new StateListAdapter(AddressActivity.this,
                                android.R.layout.simple_spinner_item,
                                state_list);
                        state_spinner.setAdapter(stateListAdapter);
                        stateListAdapter.notifyDataSetChanged();
                        Toast.makeText(AddressActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<StateList_Response> call, Throwable t) {

            }
        });


        // You can create an anonymous listener to handle the event when is selected an spinner item


    }

  /*  private void getAreaList(String state_id, String city_id) {

        Call<AreaList_Response> call = APIClient.getInterface().getAreaList(state_id, city_id);

        call.enqueue(new Callback<AreaList_Response>() {
            @Override
            public void onResponse(Call<AreaList_Response> call, Response<AreaList_Response> response) {
                if (response.isSuccessful()) {


                    if (response.body().getResponseCode().equals("200")) {

                        if (citylist.size() > 0) {
                            arealist = response.body().getData();
                            areaListAdapter = new AreaListAdapter(AddressActivity.this,
                                    android.R.layout.simple_spinner_item,
                                    arealist);
                            area_spinner.setAdapter(areaListAdapter);
                        } else {
                            arealist.clear();
                            areaListAdapter = new AreaListAdapter(AddressActivity.this,
                                    android.R.layout.simple_spinner_item,
                                    arealist);
                            area_spinner.setAdapter(areaListAdapter);
                            areaListAdapter.notifyDataSetChanged();
                        }

                    } else if (response.body().getResponseCode().equals("401")) {
                        arealist.clear();
                        arealist = new ArrayList<>();
                        areaListAdapter = new AreaListAdapter(AddressActivity.this,
                                android.R.layout.simple_spinner_item,
                                arealist);
                        area_spinner.setAdapter(areaListAdapter);
                        areaListAdapter.notifyDataSetChanged();
                        Toast.makeText(AddressActivity.this, "no Data Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AreaList_Response> call, Throwable t) {

            }
        });
    }*/

    SharedPreferences.OnSharedPreferenceChangeListener sharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals("area_name")) {
                // Write your code here
                area_name = sharedPreferences.getString("area_name", "");
            }
        }
    };
}
