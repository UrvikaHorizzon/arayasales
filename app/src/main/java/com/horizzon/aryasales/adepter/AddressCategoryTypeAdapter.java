package com.horizzon.aryasales.adepter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.horizzon.aryasales.R;
import com.horizzon.aryasales.model.AddressTypeModel;

import java.util.List;

public class AddressCategoryTypeAdapter extends ArrayAdapter<AddressTypeModel> {
    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<AddressTypeModel> values;
    List<String> categories;

    public void setItems(List<AddressTypeModel> myList) {
        this.values.clear();
        this.values.addAll(myList);
        notifyDataSetChanged();
    }

    public AddressCategoryTypeAdapter(Context context, int textViewResourceId,
                                      List<AddressTypeModel> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public AddressTypeModel getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;
        if (listItem == null) {
            listItem = LayoutInflater.from(context).inflate(R.layout.spinner_layout, parent, false);
        }

        TextView value = listItem.findViewById(R.id.item);
        value.setTextColor(Color.BLACK);
        value.setText(values.get(position).getStatus());
        return listItem;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }
}