package com.horizzon.aryasales.adepter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.horizzon.aryasales.R;
import com.horizzon.aryasales.model.AreaList_Data;

import java.util.List;

public class AreaListAdapter extends ArrayAdapter<AreaList_Data> {

    private Context context;
    // Your custom values for the spinner (User)
    private List<AreaList_Data> values;
    private  int getlist=0;

    public AreaListAdapter(Context context, int textViewResourceId,
                            List<AreaList_Data> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public void getListClear(int size){
        getlist=size;
        notifyDataSetChanged();

    }

    @Override
    public int getCount(){
        getlist=values.size();
        return getlist;
    }

    @Override
    public AreaList_Data getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;
        if (listItem == null) {
            listItem = LayoutInflater.from(context).inflate(R.layout.spinner_layout, parent, false);
        }

        TextView value = listItem.findViewById(R.id.item);
        value.setTextColor(Color.BLACK);
        value.setText(values.get(position).getName());
        return listItem;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }
}
