package com.horizzon.aryasales.adepter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CityList_Data {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("cityname")
    @Expose
    private String cityname;
    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
