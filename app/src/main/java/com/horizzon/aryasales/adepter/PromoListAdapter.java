package com.horizzon.aryasales.adepter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.aryasales.R;
import com.horizzon.aryasales.model.PromoCodeList_Data;

import java.util.List;
import java.util.zip.Inflater;

public class PromoListAdapter extends RecyclerView.Adapter<PromoListAdapter.MyViewHolder> {

    Context context;
    List<PromoCodeList_Data>list;
    PromoItemClickListner listner;

    public PromoListAdapter(FragmentActivity activity, List<PromoCodeList_Data> list) {
        this.context=activity;
        this.list=list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.promocode_list_item_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        PromoCodeList_Data data=list.get(position);
        holder.coupon_code.setText("Code : "+data.getTitle());
        holder.coupon_type.setText("Type : "+data.getDetail());
        holder.coupon_amount.setText("Discount : "+data.getValue());

        if (data.getDetail().equals("fix")){
            holder.coupon_details.setText(context.getResources().getString(R.string.rs)+" "+data.getValue()+" "+"Discount");

        }else if (data.getDetail().equals("percentage")){
            String value=data.getValue();
            String formattedString =  value + (char) 0x0025+" "+" "+"discount";
            Log.i("",formattedString);
            holder.coupon_details.setText(formattedString);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private Button use_coupon;
        private TextView coupon_code, coupon_type, coupon_amount, coupon_details;

        public MyViewHolder(@NonNull View convertView) {
            super(convertView);

            use_coupon = (Button) convertView.findViewById(R.id.use_coupon_btn);
            coupon_code = (TextView) convertView.findViewById(R.id.coupon_code);
            coupon_type = (TextView) convertView.findViewById(R.id.coupon_type);
            coupon_amount = (TextView) convertView.findViewById(R.id.coupon_amount);
            coupon_details = (TextView) convertView.findViewById(R.id.coupon_details);

            use_coupon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listner.onClick(getLayoutPosition(),list.get(getLayoutPosition()));
                }
            });
        }



    }

    public interface  PromoItemClickListner{
        public void onClick(int position,PromoCodeList_Data data);
    }

    public void setListner(PromoItemClickListner listner) {
        this.listner = listner;
    }
}
