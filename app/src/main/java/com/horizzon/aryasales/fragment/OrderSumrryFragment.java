package com.horizzon.aryasales.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.horizzon.aryasales.R;
import com.horizzon.aryasales.activity.AddressActivity;
import com.horizzon.aryasales.activity.HomeActivity;
import com.horizzon.aryasales.activity.PaypalActivity;
import com.horizzon.aryasales.activity.RazerpayActivity;
import com.horizzon.aryasales.activity.promocode_Activity;
import com.horizzon.aryasales.database.DatabaseHelper;
import com.horizzon.aryasales.database.MyCart;
import com.horizzon.aryasales.model.Address;
import com.horizzon.aryasales.model.AddressData;
import com.horizzon.aryasales.model.PaymentItem;
import com.horizzon.aryasales.model.RestResponse;
import com.horizzon.aryasales.model.ShipingData;
import com.horizzon.aryasales.model.ShippingResponse;
import com.horizzon.aryasales.model.User;
import com.horizzon.aryasales.retrofit.APIClient;
import com.horizzon.aryasales.retrofit.GetResult;
import com.horizzon.aryasales.utils.CustPrograssbar;
import com.horizzon.aryasales.utils.FragmentCallback;
import com.horizzon.aryasales.utils.SessionManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.horizzon.aryasales.utils.SessionManager.ADDRESS1;
import static com.horizzon.aryasales.utils.SessionManager.CURRUNCY;
import static com.horizzon.aryasales.utils.SessionManager.TAX;
import static com.horizzon.aryasales.utils.Utiles.isRef;
import static com.horizzon.aryasales.utils.Utiles.isSelect;
import static com.horizzon.aryasales.utils.Utiles.seletAddress;


public class OrderSumrryFragment extends Fragment implements GetResult.MyListener, FragmentCallback {

    @BindView(R.id.layout_promocode)
    View promo_code_view;
    @BindView(R.id.txt_promo_discount)
    TextView promocode_txt;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecycler_view;
    @BindView(R.id.txt_subtotal)
    TextView txtSubtotal;
    @BindView(R.id.txt_delivery)
    TextView txtDelivery;
    @BindView(R.id.txt_delevritital)
    TextView txtDelevritital;
    @BindView(R.id.txt_total)
    TextView txtTotal;
    @BindView(R.id.btn_cuntinus)
    TextView btnCuntinus;
    @BindView(R.id.lvlone)
    LinearLayout lvlone;
    @BindView(R.id.lvltwo)
    LinearLayout lvltwo;
    @BindView(R.id.txt_changeadress)
    TextView txtChangeadress;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.txt_texo)
    TextView txtTexo;
    @BindView(R.id.txt_tex)
    TextView txtTex;
    @BindView(R.id.promocodelayout)
    View promocode_view;
    @BindView(R.id.imgDeletePromo)
    ImageView imgDeletePromo;
    // TODO: Rename and change types of parameters
    private String TIME;
    private String DATA;
    private String PAYMENT;
    double TOTAL;
    public static int paymentsucsses = 0;
    public static String TragectionID = "0";
    public static boolean ISORDER = false;
    PaymentItem paymentItem;
    Address Selectaddress;
    public static final int ACTIVITYCODE = 102;
    PromoCodeFragment fragment;
    boolean promocode = true;

    double discount = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            TIME = getArguments().getString("TIME");
            DATA = getArguments().getString("DATE");
            PAYMENT = getArguments().getString("PAYMENT");
            paymentItem = (PaymentItem) getArguments().getSerializable("PAYMENTDETAILS");
        }
    }

    DatabaseHelper databaseHelper;
    List<MyCart> myCarts;
    SessionManager sessionManager;
    Unbinder unbinder;
    User user;
    CustPrograssbar custPrograssbar;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    double promocodeNew;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_sumrry, container, false);
        unbinder = ButterKnife.bind(this, view);
        custPrograssbar = new CustPrograssbar();
        databaseHelper = new DatabaseHelper(getActivity());
        sessionManager = new SessionManager(getActivity());
        HomeActivity.getInstance().setFrameMargin(0);
        user = sessionManager.getUserDetails("");
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(1, 1);
        myRecycler_view.setLayoutManager(gaggeredGridLayoutManager);
        getAddress();
        fragment = new PromoCodeFragment();
        myCarts = new ArrayList<>();
        Cursor res = databaseHelper.getAllData();
        if (res.getCount() == 0) {
            Toast.makeText(getActivity(), "NO DATA FOUND", Toast.LENGTH_LONG).show();
        }
        while (res.moveToNext()) {
            MyCart rModel = new MyCart();
            rModel.setID(res.getString(0));
            rModel.setPID(res.getString(1));
            rModel.setImage(res.getString(2));
            rModel.setTitle(res.getString(3));
            rModel.setWeight(res.getString(4));
            rModel.setCost(res.getString(5));
            rModel.setQty(res.getString(6));
            rModel.setDiscount(res.getInt(7));
            myCarts.add(rModel);
        }


        String promo_discount = sessionManager.getPromoValue();
        if (promo_discount != null && promo_discount.length() > 0) {
            discount = Double.parseDouble(promo_discount);
        }
/*
  sessionManager.clearPromoValue();

* */
        promocode_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(getContext(), promocode_Activity.class);
//               startActivityForResult(intent,ACTIVITYCODE);

                if (sessionManager.isPromoApply()) {
                    imgDeletePromo.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), "Promo Code Already Applied", Toast.LENGTH_SHORT).show();
                } else {
                    HomeActivity.getInstance().serchviewHide();
                    HomeActivity.getInstance().titleChange("Apply Promo Code");
                    HomeActivity.getInstance().callFragment(fragment);
                }

            }
        });

        imgDeletePromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("TAG", "onClick: btn :  ");
                sessionManager.clearPromoValue();
                sessionManager.clearPromoCode();
                Toast.makeText(getActivity(), "Remove Promo code.", Toast.LENGTH_SHORT).show();
            }
        });

        fragment.setFragmentCallback(this);
        return view;

    }


    private void Update(List<MyCart> mData, double discount) {
        double[] totalAmount = {0};
        int total_amount = 0;
        double dis = 0;
        for (int i = 0; i < mData.size(); i++) {
            MyCart cart = mData.get(i);
            double res = (Double.parseDouble(cart.getCost()) / 100.0f) * cart.getDiscount();
            res = Double.parseDouble(cart.getCost()) - res;
            int qrt = databaseHelper.getCard(cart.getPID(), cart.getCost());
            double temp = res * qrt;
            totalAmount[0] = totalAmount[0] + temp;
            total_amount = (int) totalAmount[0];

            dis = discount;


        }
        getTotalPayment(total_amount, new double[]{totalAmount[0]}, dis);

        Bundle bundle = new Bundle();
        double data = (double) total_amount;
        bundle.putDouble("total_price", data);
        fragment.setArguments(bundle);

    }

    private void getTotalPayment(int total_amount, double[] totalAmount, double v) {
        Call<ShippingResponse> call = APIClient.getInterface().getShipping();
        custPrograssbar.PrograssCreate(getActivity());
        call.enqueue(new Callback<ShippingResponse>() {

            @Override
            public void onResponse(Call<ShippingResponse> call, Response<ShippingResponse> response) {
                if (response.isSuccessful()) {
                    custPrograssbar.ClosePrograssBar();

                    List<ShipingData> data = response.body().getData();
                    for (int i = 0; i < data.size(); i++) {

                        ShipingData details = data.get(i);
                        int fromAmount = Integer.parseInt(details.getFromAmount());
                        int toAmount = Integer.parseInt(details.getToAmount());

                        if (total_amount >= fromAmount && total_amount <= toAmount) {
                            float shippingcharge = Float.parseFloat(details.getShippingValue());

                            if (discount > 0) {
                                totalAmount[0] = totalAmount[0] - v;
                                promo_code_view.setVisibility(View.VISIBLE);
                                promocode_txt.setText(sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(discount));
                                Log.d("TAG", "onResponse: discount : promo : " + discount);
                                promocodeNew = discount;
                            }

                            txtSubtotal.setText(sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(totalAmount[0]));
                            double tex = Double.parseDouble(sessionManager.getStringData(TAX));
                            txtTexo.setText("GSTIN:(" + tex + "%)");
                            tex = (totalAmount[0] / 100.0f) * tex;
                            txtTex.setText(sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(tex));
                            totalAmount[0] = totalAmount[0] + tex;

                            if (PAYMENT.equalsIgnoreCase(getResources().getString(R.string.pic_myslf))) {
                                txtDelivery.setVisibility(View.VISIBLE);
                                txtDelevritital.setVisibility(View.VISIBLE);
                                txtDelivery.setText(sessionManager.getStringData(CURRUNCY) + "0");
                            } else {
                                totalAmount[0] = totalAmount[0] + shippingcharge;
                                txtDelivery.setText(sessionManager.getStringData(CURRUNCY) + shippingcharge);
                            }


                            txtTotal.setText(sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(totalAmount[0]));
                            btnCuntinus.setText("Place Order - " + sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(totalAmount[0]));
                            TOTAL = totalAmount[0];
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ShippingResponse> call, Throwable t) {
                custPrograssbar.ClosePrograssBar();
            }
        });


//        txtSubtotal.setText(sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(totalAmount[0]));
//        double tex = Double.parseDouble(sessionManager.getStringData(TAX));
//        txtTexo.setText("Service Tax(" + tex + "%)");
//        tex = (totalAmount[0] / 100.0f) * tex;
//        txtTex.setText(sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(tex));
//        totalAmount[0] = totalAmount[0] + tex;
//
//        if (PAYMENT.equalsIgnoreCase(getResources().getString(R.string.pic_myslf))) {
//            txtDelivery.setVisibility(View.VISIBLE);
//            txtDelevritital.setVisibility(View.VISIBLE);
//            txtDelivery.setText(sessionManager.getStringData(CURRUNCY)  + "0");
//        } else {
//            totalAmount[0] = totalAmount[0] + Selectaddress.getDeliveryCharge();
//            txtDelivery.setText(sessionManager.getStringData(CURRUNCY) + Selectaddress.getDeliveryCharge());
//        }
//
//
//        txtTotal.setText(sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(totalAmount[0]));
//        btnCuntinus.setText("Place Order - " + sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(totalAmount[0]));
//        TOTAL = totalAmount[0];
    }


    @Override
    public void onDataSent(Double yourData) {
        Update(myCarts, yourData);
        Toast.makeText(getActivity(), "You Save" + " " + yourData + " " + "Rupees", Toast.LENGTH_SHORT).show();
    }


    public class ItemAdp extends RecyclerView.Adapter<ItemAdp.ViewHolder> {
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        private List<MyCart> mData;
        private LayoutInflater mInflater;
        Context mContext;
        SessionManager sessionManager;

        public ItemAdp(Context context, List<MyCart> data) {
            this.mInflater = LayoutInflater.from(context);
            this.mData = data;
            this.mContext = context;
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            sessionManager = new SessionManager(mContext);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.custome_sumrry, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int i) {
            MyCart cart = mData.get(i);
            Glide.with(getActivity()).load(APIClient.Base_URL + "/" + cart.getImage()).thumbnail(Glide.with(getActivity()).load(R.drawable.lodingimage)).into(holder.imgIcon);
            double res = (Double.parseDouble(cart.getCost()) / 100.0f) * cart.getDiscount();
            res = Double.parseDouble(cart.getCost()) - res;
            holder.txtTitle.setText("" + cart.getTitle());
            MyCart myCart = new MyCart();
            myCart.setPID(cart.getPID());
            myCart.setImage(cart.getImage());
            myCart.setTitle(cart.getTitle());
            myCart.setWeight(cart.getWeight());
            myCart.setCost(cart.getCost());
            int qrt = helper.getCard(myCart.getPID(), myCart.getCost());
            holder.txtPriceanditem.setText(qrt + " item x " + sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(res));
            double temp = res * qrt;
            holder.txtPrice.setText(sessionManager.getStringData(CURRUNCY) + new DecimalFormat("##.##").format(temp));

        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.img_icon)
            ImageView imgIcon;
            @BindView(R.id.txt_title)
            TextView txtTitle;
            @BindView(R.id.txt_priceanditem)
            TextView txtPriceanditem;
            @BindView(R.id.txt_price)
            TextView txtPrice;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

        }


    }


    private void OrderPlace(JSONArray jsonArray) {
        if (Selectaddress == null) {
            getAddress();
            return;
        }
        custPrograssbar.PrograssCreate(getActivity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            jsonObject.put("timesloat", TIME);
            jsonObject.put("ddate", DATA);
            jsonObject.put("total", TOTAL);
            jsonObject.put("p_method", PAYMENT);
            jsonObject.put("promocode_amount", promocodeNew);
            jsonObject.put("address_id", Selectaddress.getId());
            jsonObject.put("tax", sessionManager.getStringData(TAX));
            jsonObject.put("tid", TragectionID);
            jsonObject.put("pname", jsonArray);
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().Order((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void callback(JsonObject result, String callNo) {
        try {
            custPrograssbar.ClosePrograssBar();
            if (callNo.equalsIgnoreCase("1")) {
                sessionManager.clearPromoValue();
                Gson gson = new Gson();
                RestResponse response = gson.fromJson(result.toString(), RestResponse.class);
                Toast.makeText(getActivity(), "" + response.getResponseMsg(), Toast.LENGTH_LONG).show();
                if (response.getResult().equals("true")) {
                    lvlone.setVisibility(View.GONE);
                    lvltwo.setVisibility(View.VISIBLE);
                    databaseHelper.DeleteCard();
                    ISORDER = true;
                }
            } else if (callNo.equalsIgnoreCase("2323")) {
                Gson gson = new Gson();
                btnCuntinus.setClickable(true);

                AddressData addressData = gson.fromJson(result.toString(), AddressData.class);
                if (addressData.getResult().equalsIgnoreCase("true")) {
                    if (addressData.getResultData().size() != 0) {
                        Selectaddress = addressData.getResultData().get(seletAddress);
                        if (Selectaddress.isIS_UPDATE_NEED()) {
                            Toast.makeText(getActivity(), "Please Update Your Area Name.Because It's Not match with Our Delivery Location", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getActivity(), AddressActivity.class).putExtra("MyClass", Selectaddress));
                        } else {
                            txtAddress.setText(Selectaddress.getHno() + "," + Selectaddress.getSociety() + "," + Selectaddress.getArea() + "," + Selectaddress.getLandmark() + "," + Selectaddress.getName());
                            ItemAdp itemAdp = new ItemAdp(getActivity(), myCarts);
                            myRecycler_view.setAdapter(itemAdp);
                            Update(myCarts, discount);
                        }


                    } else {
                        Toast.makeText(getActivity(), "Please add your address ", Toast.LENGTH_LONG).show();

                        AddressFragment fragment = new AddressFragment();
                        HomeActivity.getInstance().callFragment(fragment);
                    }
                } else {
                    Toast.makeText(getActivity(), "Please add your address ", Toast.LENGTH_LONG).show();

                    AddressFragment fragment = new AddressFragment();
                    HomeActivity.getInstance().callFragment(fragment);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.txt_changeadress, R.id.btn_cuntinus, R.id.txt_trackorder})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_changeadress:
                isSelect = true;
                AddressFragment fragment = new AddressFragment();
                HomeActivity.getInstance().callFragment(fragment);
                break;
            case R.id.txt_trackorder:
                ClearFragment();
                break;
            case R.id.btn_cuntinus:
                btnCuntinus.setClickable(false);
                if (PAYMENT.equalsIgnoreCase("Razorpay")) {
                    int temtoal = (int) Math.round(TOTAL);
                    startActivity(new Intent(getActivity(), RazerpayActivity.class).putExtra("amount", temtoal).putExtra("detail", paymentItem));
                } else if (PAYMENT.equalsIgnoreCase("Paypal")) {
                    startActivity(new Intent(getActivity(), PaypalActivity.class).putExtra("amount", TOTAL).putExtra("detail", paymentItem));

                } else if (PAYMENT.equalsIgnoreCase("Cash On Delivery") || PAYMENT.equalsIgnoreCase("Pickup Myself")) {
                    sendorderServer();
                }

                break;
            default:
                break;
        }
    }

    public void ClearFragment() {
        sessionManager = new SessionManager(getActivity());
        User user1 = sessionManager.getUserDetails("");
        HomeActivity.getInstance().titleChange("Hello " + user1.getName());
        MyOrderFragment homeFragment = new MyOrderFragment();
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getFragmentManager().beginTransaction().replace(R.id.fragment_frame, homeFragment).addToBackStack(null).commit();
    }

    private void sendorderServer() {
        Cursor res = databaseHelper.getAllData();
        if (res.getCount() == 0) {
            return;
        }
        if (user.getArea() != null || user.getSociety() != null || user.getHno() != null || user.getMobile() != null) {
            JSONArray jsonArray = new JSONArray();
            while (res.moveToNext()) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", res.getString(0));
                    jsonObject.put("pid", res.getString(1));
                    jsonObject.put("image", res.getString(2));
                    jsonObject.put("title", res.getString(3));
                    jsonObject.put("weight", res.getString(4));
                    jsonObject.put("cost", res.getString(5));
                    jsonObject.put("qty", res.getString(6));
                    jsonArray.put(jsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            OrderPlace(jsonArray);
        } else {
            startActivity(new Intent(getActivity(), AddressActivity.class));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        HomeActivity.getInstance().titleChange("Placed Order");
        HomeActivity.getInstance().serchviewHide();
        HomeActivity.getInstance().setFrameMargin(0);

        try {
            if (btnCuntinus != null) {
                btnCuntinus.setClickable(true);
            }
            if (paymentsucsses == 1) {
                paymentsucsses = 0;
                sendorderServer();
            }
            if (sessionManager != null) {
                Selectaddress = sessionManager.getAddress(ADDRESS1);
                if (Selectaddress != null) {
                    txtAddress.setText(Selectaddress.getHno() + "," + Selectaddress.getSociety() + "," + Selectaddress.getArea() + "," + Selectaddress.getLandmark() + "," + Selectaddress.getName());
                    Update(myCarts, discount);

                    if (isRef) {
                        isRef = false;
                        ItemAdp itemAdp = new ItemAdp(getActivity(), myCarts);
                        myRecycler_view.setAdapter(itemAdp);
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAddress() {
        custPrograssbar.PrograssCreate(getActivity());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            JsonParser jsonParser = new JsonParser();
            Call<JsonObject> call = APIClient.getInterface().getAddress((JsonObject) jsonParser.parse(jsonObject.toString()));
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "2323");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
