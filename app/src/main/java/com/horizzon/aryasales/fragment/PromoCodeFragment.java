package com.horizzon.aryasales.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.aryasales.R;
import com.horizzon.aryasales.activity.HomeActivity;
import com.horizzon.aryasales.adepter.PromoListAdapter;
import com.horizzon.aryasales.database.DatabaseHelper;
import com.horizzon.aryasales.database.MyCart;
import com.horizzon.aryasales.model.PaymentItem;
import com.horizzon.aryasales.model.PromoCodeList_Data;
import com.horizzon.aryasales.model.PromoCodeList_Response;
import com.horizzon.aryasales.model.Promocode_Response;
import com.horizzon.aryasales.model.Promocode_Response_Data;
import com.horizzon.aryasales.model.ShippingResponse;
import com.horizzon.aryasales.model.User;
import com.horizzon.aryasales.retrofit.APIClient;
import com.horizzon.aryasales.utils.CustPrograssbar;
import com.horizzon.aryasales.utils.FragmentCallback;
import com.horizzon.aryasales.utils.SessionManager;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromoCodeFragment extends Fragment implements View.OnClickListener, PromoListAdapter.PromoItemClickListner {


    Unbinder unbinder;
    CustPrograssbar custPrograssbar;
    private FragmentCallback fragmentCallback;
    private double total_price;

    @BindView(R.id.avlailable_promotxt)
    TextView available_coupon_code;
    @BindView(R.id.promo_txt)
    EditText promocode_txt;
    @BindView(R.id.promo_code_apply_btn)
    Button promo_code_apply_btn;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.header_no_data)
     TextView no_data_found_header;


    DatabaseHelper databaseHelper;
    SessionManager sessionManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.promocode_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        custPrograssbar = new CustPrograssbar();
        promo_code_apply_btn.setOnClickListener(this);
        databaseHelper = new DatabaseHelper(getActivity());
        sessionManager = new SessionManager(getActivity());


        getPromocodeList();

        return view;

    }

    private void getPromocodeList() {
        custPrograssbar.PrograssCreate(getActivity());
        Call<PromoCodeList_Response> call=APIClient.getInterface().getPromoCodeList();
        call.enqueue(new Callback<PromoCodeList_Response>() {
            @Override
            public void onResponse(Call<PromoCodeList_Response> call, Response<PromoCodeList_Response> response) {
                if (response.isSuccessful() && response.body().getResponseCode().equals("200")){

                    List<PromoCodeList_Data>list=response.body().getData();
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());

                    if (list.size()>0){
                        custPrograssbar.ClosePrograssBar();
                        PromoListAdapter adapter=new PromoListAdapter(getActivity(),list);
                        recyclerView.setAdapter(adapter);
                       adapter.setListner(PromoCodeFragment.this);

                    }else {
                        no_data_found_header.setVisibility(View.VISIBLE);
                        custPrograssbar.ClosePrograssBar();
                    }

                }
            }

            @Override
            public void onFailure(Call<PromoCodeList_Response> call, Throwable t) {
                custPrograssbar.ClosePrograssBar();
                Toast.makeText(getContext(), "Network failure", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeActivity.getInstance().serchviewHide();
        HomeActivity.getInstance().setFrameMargin(0);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.promo_code_apply_btn:
                custPrograssbar.PrograssCreate(getActivity());
                String getText=promocode_txt.getText().toString();
                getPromocodeValue();
                sessionManager.ApplyPromoCode(true);
                break;
        }
    }

    private void getPromocodeValue() {
        User user=sessionManager.getUserDetails("");
        Call<Promocode_Response> call = APIClient.getInterface().getPromoCode(promocode_txt.getText().toString(),user.getId());
        call.enqueue(new Callback<Promocode_Response>() {
            @Override
            public void onResponse(Call<Promocode_Response> call, Response<Promocode_Response> response) {
                if (response.isSuccessful()) {
                    custPrograssbar.ClosePrograssBar();

                    if (response.body().getResponseCode().equals("200")){
                        if (response.body().getResponseMsg().equals("Promocode is applied Successfully!!!")) {

                            List<Promocode_Response_Data> list = response.body().getData();
                                Promocode_Response_Data data = list.get(0);

                                double promocode = Double.parseDouble(data.getValue());
                                if (data.getDetail().equals("fix")) {
                                    sessionManager.setPromoValue(String.valueOf(promocode));

                                    fragmentCallback.onDataSent(promocode);
                                    getActivity().onBackPressed();


                                } else if (data.getDetail().equals("percentage")) {
                                    total_price=getArguments().getDouble("total_price");
                                    double pricevalue = (total_price / 100.0f) * promocode;
                                    sessionManager.setPromoValue(String.valueOf(pricevalue));
                                    fragmentCallback.onDataSent(pricevalue);
                                    getActivity().onBackPressed();

                                }


                        }

                        else if (response.body().getResponseMsg().equals("This promocode is already used!!!")){

                            Toast.makeText(getActivity(), ""+response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }

                    }else if (response.body().getResponseCode().equals("401")){
                        Toast.makeText(getActivity(), "Invalid Promo Code", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Promocode_Response> call, Throwable t) {

                Toast.makeText(getContext(), "error" + t.getMessage(), Toast.LENGTH_SHORT).show();
                custPrograssbar.ClosePrograssBar();
            }
        });
    }

    public void setFragmentCallback(FragmentCallback callback) {
        this.fragmentCallback = callback;
    }

    @Override
    public void onClick(int position, PromoCodeList_Data data) {
        promocode_txt.setText(data.getTitle());
    }
}
