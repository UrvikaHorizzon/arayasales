
package com.horizzon.aryasales.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Address implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("hno")
    @Expose
    private String hno;
    @SerializedName("society")
    @Expose
    private String society;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("gts_number")
    @Expose
    private String gtsNumber;
    @SerializedName("shop_name")
    @Expose
    private String shopName;
    @SerializedName("delivery_charge")
    @Expose
    private Object deliveryCharge;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("landmark")
    @Expose
    private String landmark;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("IS_UPDATE_NEED")
    private boolean IS_UPDATE_NEED;
    public boolean isIS_UPDATE_NEED() {
        return IS_UPDATE_NEED;
    }

    public void setIS_UPDATE_NEED(boolean IS_UPDATE_NEED) {
        this.IS_UPDATE_NEED = IS_UPDATE_NEED;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getHno() {
        return hno;
    }

    public void setHno(String hno) {
        this.hno = hno;
    }

    public String getSociety() {
        return society;
    }

    public void setSociety(String society) {
        this.society = society;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getGTSNumber() {
        return gtsNumber;
    }

    public void setGTSNumber(String gTSNumber) {
        this.gtsNumber = gTSNumber;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Object getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(Object deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}


    /*@SerializedName("area")
    private String area;
    @SerializedName("hno")
    private String hno;
    @SerializedName("id")
    private String id;
    @SerializedName("landmark")
    private String landmark;
    @SerializedName("name")
    private String name;
    @SerializedName("pincode")
    private String pincode;
    @SerializedName("society")
    private String society;
    @SerializedName("status")
    private String status;
    @SerializedName("type")
    private String type;
    @SerializedName("uid")
    private String uid;

    @SerializedName("city")
    private String city;

    @SerializedName("state")
    private String state;

    @SerializedName("GTS_number")
    private String GSTNumber;

    @SerializedName("ShopName")
    private String ShopName;
    @SerializedName("delivery_charge")
    private float deliveryCharge;
    @SerializedName("IS_UPDATE_NEED")
    private boolean IS_UPDATE_NEED;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGSTNumber() {
        return GSTNumber;
    }

    public void setGSTNumber(String GSTNumber) {
        this.GSTNumber = GSTNumber;
    }

    public String getShopName() {
        return ShopName;
    }

    public void setShopName(String shopName) {
        ShopName = shopName;
    }

    public boolean isIS_UPDATE_NEED() {
        return IS_UPDATE_NEED;
    }

    public void setIS_UPDATE_NEED(boolean IS_UPDATE_NEED) {
        this.IS_UPDATE_NEED = IS_UPDATE_NEED;
    }

    public float getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(int deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getHno() {
        return hno;
    }

    public void setHno(String hno) {
        this.hno = hno;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getSociety() {
        return society;
    }

    public void setSociety(String society) {
        this.society = society;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }*/
//}
