package com.horizzon.aryasales.model;

public class AddressTypeModel {

    private String categoryName;

    public AddressTypeModel(String s) {
        this.categoryName = s;
    }

    public String getStatus() {
        return categoryName;
    }

    public void setStatus(String status) {
        this.categoryName = status;
    }
    @Override
    public String toString() {
        return this.categoryName; // What to display in the Spinner list.
    }
}
