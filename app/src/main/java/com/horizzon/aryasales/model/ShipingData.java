package com.horizzon.aryasales.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShipingData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("from_amount")
    @Expose
    private String fromAmount;
    @SerializedName("to_amount")
    @Expose
    private String toAmount;
    @SerializedName("shipping_value")
    @Expose
    private String shippingValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(String fromAmount) {
        this.fromAmount = fromAmount;
    }

    public String getToAmount() {
        return toAmount;
    }

    public void setToAmount(String toAmount) {
        this.toAmount = toAmount;
    }

    public String getShippingValue() {
        return shippingValue;
    }

    public void setShippingValue(String shippingValue) {
        this.shippingValue = shippingValue;
    }
}
