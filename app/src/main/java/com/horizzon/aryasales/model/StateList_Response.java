package com.horizzon.aryasales.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horizzon.aryasales.adepter.CityList_Data;

import java.util.List;

public class StateList_Response {

    @SerializedName("Data")
    @Expose
    private List<StateList_Data> data = null;
    @SerializedName("ResponseCode")
    @Expose
    private String responseCode;
    @SerializedName("Result")
    @Expose
    private String result;
    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;


    public List<StateList_Data> getData() {
        return data;
    }

    public void setData(List<StateList_Data> data) {
        this.data = data;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }


}
