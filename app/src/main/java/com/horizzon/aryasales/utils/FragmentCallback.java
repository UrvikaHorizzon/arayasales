package com.horizzon.aryasales.utils;

public interface FragmentCallback {
    void onDataSent(Double yourData);
}
