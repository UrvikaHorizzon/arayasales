package com.horizzon.aryasales.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.horizzon.aryasales.model.Address;
import com.horizzon.aryasales.model.User;
import com.google.gson.Gson;

public class SessionManager {
    private final SharedPreferences mPrefs;
    SharedPreferences.Editor mEditor;
    public static String LOGIN = "login";
    public static String ISOPEN = "isopen";
    public static String USERDATA = "Userdata";
    public static String ADDRESS1 = "address";
    public static boolean ISCART = false;
    public static String AREA = "area";
    public static String CURRUNCY = "currncy";
    public static String PRIVACY = "privacy_policy";
    public static String TREMSCODITION = "tremcodition";
    public static String ABOUT_US = "about_us";
    public static String CONTACT_US = "contact_us";
    public static String O_MIN = "o_min";
    public static String RAZ_KEY = "raz_key";
    public static String TAX = "tax";
    public static String PROMOCODE = "promo_code_apply";
    public static String PROMOVALUE = "value";
    public static String STATE_ID = "state_id";
    public static String CITY_ID = "city_id";
    public static String AREA_ID = "area_id";
    public static String STATE_NAME = "state_name";
    public static String CITY_NAME = "city_name";
    public static String AREA_NAME = "area_name";

    public SessionManager(Context context) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mPrefs.edit();
    }

    public void setStringData(String key, String val) {
        mEditor.putString(key, val);
        mEditor.commit();
    }

    public String getStringData(String key) {
        return mPrefs.getString(key, "");
    }

    public void setBooleanData(String key, Boolean val) {
        mEditor.putBoolean(key, val);
        mEditor.commit();
    }

    public boolean getBooleanData(String key) {
        return mPrefs.getBoolean(key, false);
    }

    public void setIntData(String key, int val) {
        mEditor.putInt(key, val);
        mEditor.commit();
    }

    public int getIntData(String key) {
        return mPrefs.getInt(key, 0);
    }

    public void setUserDetails(String key, User val) {
        mEditor.putString(USERDATA, new Gson().toJson(val));
        mEditor.commit();
    }

    public User getUserDetails(String key) {
        return new Gson().fromJson(mPrefs.getString(USERDATA, ""), User.class);
    }

    public void setAddress(String key, Address val) {
        mEditor.putString(ADDRESS1, new Gson().toJson(val));
        mEditor.commit();
    }

    public Address getAddress(String key) {
        return new Gson().fromJson(mPrefs.getString(ADDRESS1, ""), Address.class);
    }

    public void logoutUser() {
        mEditor.clear();
        mEditor.commit();

    }

    public void ApplyPromoCode(boolean promo) {
        mEditor.putBoolean(PROMOCODE, promo);
        mEditor.commit();
    }

    public void clearPromoCode() {
        mEditor.remove(PROMOCODE);
        mEditor.commit();
    }

    public boolean isPromoApply() {
        return mPrefs.getBoolean(PROMOCODE, false);
    }


    public void setPromoValue(String promocode) {
        mEditor.putString(PROMOVALUE, promocode);
        mEditor.commit();
    }

    public String getPromoValue() {
        return mPrefs.getString(PROMOVALUE, "");
    }

    public void clearPromoValue() {
        mEditor.remove(PROMOVALUE);
        mEditor.commit();
    }

    public void setStateId(String id,String name) {
        mEditor.putString(STATE_ID, id);
        mEditor.putString(STATE_NAME, name);
        mEditor.commit();
    }

    public void setCityId(String id,String cityname) {
        mEditor.putString(CITY_ID, id);
        mEditor.putString(CITY_NAME,cityname);
        mEditor.commit();
    }

    public void setAreaId(String id,String araname) {
        mEditor.putString(AREA_ID, id);
        mEditor.putString(AREA_NAME,araname);
        mEditor.commit();
    }

    public String getStateId(){
        return mPrefs.getString(STATE_ID,"");
    }

    public String getCityId(){
        return mPrefs.getString(CITY_ID,"");
    }

    public String getAreaId(){
        return mPrefs.getString(AREA_ID,"");
    }


    public String getStateName(){
        return mPrefs.getString(STATE_NAME,"");
    }

    public String getCityName(){
        return mPrefs.getString(CITY_NAME,"");
    }

    public String getAreaName(){
        return mPrefs.getString(AREA_NAME,"");
    }
}
